FROM maven:3.9-eclipse-temurin-17-alpine AS MAVEN_BUILD

COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/
RUN mvn clean package

FROM amazoncorretto:17-alpine3.16

WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/*.jar /app/ketmon.jar

ENTRYPOINT ["java", "-jar", "ketmon.jar"]